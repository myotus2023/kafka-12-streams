package ru.ovechkin.kafka12streams;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.internals.TimeWindow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafkaStreams;

import java.time.Duration;


@SpringBootApplication
@Slf4j
@EnableKafkaStreams
public class Kafka12streamsApplication {
    private static final Serde<String> STRING_SERDE = Serdes.String();
    @Value("${session-window-ms}")
    private int sessionWindowMs;

    public static void main(String[] args) {
        SpringApplication.run(Kafka12streamsApplication.class, args);
    }

    @Autowired
    public void run(StreamsBuilder streamsBuilder) {
        streamsBuilder
                .stream("events", Consumed.with(STRING_SERDE, STRING_SERDE))
                .groupByKey()
                .windowedBy(SessionWindows.ofInactivityGapWithNoGrace(Duration.ofMillis(sessionWindowMs)))
                .count()
                .toStream()
                .map((key, value) -> new KeyValue<>(key.key(), value + " time from " + key.window().startTime() + " to " + key.window().endTime()))
                .to("output-events", Produced.with(STRING_SERDE, STRING_SERDE));
    }

}
