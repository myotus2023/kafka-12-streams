Сервис с использованием kafka streams

1. Читает сообщения из топика events по ключам.
2. Если между сообщениями с одинаковым ключом разница больше session-window-ms секунд - создаётся новая группа
3. Подсчитывает сообщения в рамках одной группы и заносит сведения в топи output-events